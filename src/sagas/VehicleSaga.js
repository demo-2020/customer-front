import {put, call} from 'redux-saga/effects'
import RetrieveVehiclesFailed from "../actions/RetrieveVehiclesFailed";
import { vehicleService } from "../constants/Urls";
import axios from "axios";
import RetrieveVehiclesSuccessful from "../actions/RetrieveVehiclesSuccessful";

const retrieveVehiclesApi = () => axios.get(vehicleService.vehicles)

export function* fetchVehicles(action) {
    try {
        const response = yield call(retrieveVehiclesApi, null)
        yield put(RetrieveVehiclesSuccessful(response.data._embedded.vehicles))
    } catch (e) {
        yield put(RetrieveVehiclesFailed(e.message))
    }
}
