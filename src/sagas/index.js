import {fetchVehicles} from "./VehicleSaga";
import {all, takeLatest} from 'redux-saga/effects'
import RetrieveVehicles from "../actions/RetrieveVehicles";

export default function * rootSaga () {
    yield all([
        takeLatest(RetrieveVehicles.type, fetchVehicles)
    ])
}
