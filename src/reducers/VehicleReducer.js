import RetrieveVehiclesSuccessful from "../actions/RetrieveVehiclesSuccessful";
import RetrieveVehiclesFailed from "../actions/RetrieveVehiclesFailed";

export default (state = [], action) => {
    switch(action.type){
        case RetrieveVehiclesSuccessful([]).type:
            return action.vehicles
        case RetrieveVehiclesFailed.type:
            return null
        default:
            return []
    }
}
