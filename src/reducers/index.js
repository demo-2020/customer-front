import {combineReducers} from "redux";
import VehicleReducer from "./VehicleReducer";

export default combineReducers({
    vehicles: VehicleReducer
})
