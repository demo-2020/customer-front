import React from 'react'
import {Card} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    card: {
        borderRadius: `0 0 ${theme.spacing(2)}px 0`,
        margin: theme.spacing(2),
    },
    media: {
        height: 300,
    }
}))

const VehicleCard = ({make, model, cost, seats}) => {
    const classes = useStyles()

    return <Card className={classes.card}>
        <CardActionArea>
            <CardMedia
                className={classes.media}
                image={'images/tesla_model_s.jpg'}
            />
        </CardActionArea>
        <CardContent>
            <List>
                <ListItem>
                    <ListItemText primary={make} secondary={model}/>
                </ListItem>
                <ListItem>
                    <ListItemText primary={cost} secondary={seats + ' Seats'}/>
                </ListItem>
            </List>
        </CardContent>
        <CardActions>
            <Button>
                Book
            </Button>
            <Button>
                Details
            </Button>
        </CardActions>
    </Card>
}

export default VehicleCard
