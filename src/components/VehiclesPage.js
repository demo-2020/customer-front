import React, {useEffect} from 'react'
import {Container} from "@material-ui/core";
import VehicleList from "./VehicleList";
import {useDispatch, useSelector} from "react-redux";
import RetrieveVehicles from "../actions/RetrieveVehicles";

const VehiclesPage = () => {

    const dispatch = useDispatch()
    const vehicles = useSelector(state => state.vehicles)

    useEffect(() => {
        dispatch(RetrieveVehicles)
    }, [])

    return <Container>
        <VehicleList vehicles={vehicles}/>
    </Container>
}

export default VehiclesPage
