import React from 'react'
import BasePage from "./BasePage";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {MuiThemeProvider as ThemeProvider} from '@material-ui/core/styles';
import {purple} from "@material-ui/core/colors";
import VehiclesPage from "./VehiclesPage";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import HomePage from "./HomePage";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: purple[500]
        }
    }
})

const App = () => {

    return <ThemeProvider theme={theme}>
        <BrowserRouter>
            <BasePage>
                <Switch>
                    <Route exact path={'/'}>
                        <HomePage/>
                    </Route>
                    <Route exact path={'/vehicles'}>
                        <VehiclesPage/>
                    </Route>
                </Switch>
            </BasePage>
        </BrowserRouter>
    </ThemeProvider>;
}

export default App
