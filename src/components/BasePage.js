import React from 'react'
import Container from "@material-ui/core/Container";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Button from "@material-ui/core/Button";
import {useHistory} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
        appBar: {
            borderRadius: `0 0 ${theme.spacing(2)}px 0`,
            marginRight: theme.spacing(2),
            width: `calc(100% - ${theme.spacing(2)}px)`,
        },
        button: {
            color: theme.palette.primary.contrastText,
            marginLeft: theme.spacing(2),
        }
}))

const BasePage = ({children}) => {
    const classes = useStyles()
    const history = useHistory()

    const handleOnClickVehicles = () => history.push('/vehicles')

    const handleOnClickHome = () => history.push('/')

    return <>
        <AppBar className={classes.appBar}>
            <Toolbar>
                <Typography variant={'h6'}>
                    Fledge
                </Typography>
                <Button className={classes.button} onClick={handleOnClickHome}>
                    Home
                </Button>
                <Button className={classes.button} onClick={handleOnClickVehicles}>
                    Vehicles
                </Button>
            </Toolbar>
        </AppBar>
        <Toolbar/>
        <Container>
            {
                children
            }
        </Container>
    </>
}

export default BasePage
