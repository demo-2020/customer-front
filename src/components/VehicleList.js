import React from 'react'
import Grid from "@material-ui/core/Grid";
import VehicleCard from "./VehicleCard";

const VehicleList = ({vehicles}) => {

    return <Grid container>
        {
            vehicles.map((vehicle, index) => <Grid key={index} item xs={3}>
                <VehicleCard {...vehicle}/>
            </Grid>)
        }
    </Grid>
}

export default VehicleList
