import React from 'react'
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";

const Home = () => <Container>
    <Typography>
        Home
    </Typography>
</Container>

export default Home
