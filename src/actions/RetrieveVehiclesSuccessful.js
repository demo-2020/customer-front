export default (vehicles) => ({
    type: 'RETRIEVE_VEHICLES_SUCCESSFUL',
    vehicles
})
