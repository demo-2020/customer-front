export default (message) => ({
    type: 'RETRIEVE_VEHICLES_FAILED',
    message: message,
})
