FROM node

WORKDIR /app

COPY package.json /app
COPY src /app/src
COPY public /app/public

RUN npm install
RUN npm run build
RUN npm i -g serve

CMD ["serve", "-s", "build"]